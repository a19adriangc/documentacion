# JTextField personalizado
***
El objetivo será contruir un nuevo componente **JTextField**. Este nuevo componente tendrá un nuevo campo que nos permita seleccionar si el contenido se trata de número enteros o no.
Usarémos el IDE **NetBeans** en le elaboración del componente.
Únicamente es necesaria la realización de una clase que *extienda* de [JTextField](https://docs.oracle.com/javase/7/docs/api/javax/swing/JTextField.html) e *implemente* [Serializable](https://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html).
También necesitaremos incluir un atributo *boolean*, con su *set* y *get* correspondiente. Y un constructor sin parámetros.
***
~~~
public class JTextFieldCustom extends JTextField implements Serializable {
	private boolean soloEnteros = false;
	
	public JTextFieldCustom() {
    }
	
	public boolean isSoloEnteros() {
        return soloEnteros;
    }

    public void setSoloEnteros(boolean soloEnteros) {
        this.soloEnteros = soloEnteros;
    }
}
~~~
***
Este tipo de clase es llamada **JavaBean**. Tiene las siguientes características.
* Se implementa **Serializable**.
* Existe un constructor sin parámetros.
* Sus atributos son privados.
* Dichos atributos cuentan cos sus respectivos *set* y *get*.
***
Si dejásemos esto así simplemente tendríamos un nuevo componente **Bean** con un campo llamado soloEnteros, de estilo checkbox, que no tiene ninguna utilidad. Necesitamos añadir algo más a nuestra clase. Específicamente un [KeyListener](https://docs.oracle.com/javase/7/docs/api/java/awt/event/KeyListener.html).
El siguiente código lo introduciremos dentro del constructor.
Veremos que nos indica que debemos sobreescribir varios métodos. Sobreescribimos todos pero únicamente usaremos el método **keyTyped**.
Nos quedaría algo como lo siguiente.
***
~~~
this.addKeyListener(new KeyListener() {

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
        public void keyTyped(KeyEvent e) {
            if(soloEnteros) {
                char c = e.getKeyChar();
				if (!(c >= '0' && c <= '9') && c != KeyEvent.VK_BACK_SPACE && c != KeyEvent.VK_DELETE) {
					e.consume();
				}
            }
        }

    @Override
    public void keyReleased(KeyEvent e) {
    }
            
});
~~~
***
El metódo **keyTyped** se llama cada vez que se pulsa una tecla.
En caso de que el boolean **soloEnteros** esté a false, no haremos nada. Si se encuentra en estado true. Solo permitimos que se tecleen **numeros 0-9**, **BACK_SPACE**  y **DELETE**.
***
Y ya hemos terminado. Solo nos queda añadir el nuevo componente a la paleta **Beans** y usarlo cuando queramos en otro proyecto.
***Click derecho en la clase > Tools > Add to Palette... > Beans***
![Añadir a paleta](./images/addPalette.png)
***