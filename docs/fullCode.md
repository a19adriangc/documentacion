# Full Code de JTextField personalizado
***
~~~
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.Serializable;
import javax.swing.JTextField;

public class JTextFieldCustom extends JTextField implements Serializable {
 
    private boolean soloEnteros = false;
    
    public JTextFieldCustom() {
        this.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
                if(soloEnteros) {
                    char c = e.getKeyChar();
                    if (!(c >= '0' && c <= '9') && c != KeyEvent.VK_BACK_SPACE && c != KeyEvent.VK_DELETE) {
                        e.consume();
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
            
        });
    }

    public boolean isSoloEnteros() {
        return soloEnteros;
    }

    public void setSoloEnteros(boolean soloEnteros) {
        this.soloEnteros = soloEnteros;
        this.setText(soloEnteros ? "" : "Introduce un texto" );
    }
    
}
~~~
***
